-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3309
-- Generation Time: Aug 05, 2019 at 02:43 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kemiskinan`
--

-- --------------------------------------------------------

--
-- Table structure for table `aset`
--

CREATE TABLE `aset` (
  `id_Aset` int(10) UNSIGNED NOT NULL,
  `OptionAset_id_OptionAset` int(10) UNSIGNED NOT NULL,
  `id_AsetBergerak` int(10) UNSIGNED DEFAULT NULL,
  `id_AsetTidakBergerak` int(10) UNSIGNED DEFAULT NULL,
  `id_Ternak` int(10) UNSIGNED DEFAULT NULL,
  `id_Usaha` int(10) UNSIGNED DEFAULT NULL,
  `id_Program` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asetbergerak`
--

CREATE TABLE `asetbergerak` (
  `id_AsetBergerak` int(10) UNSIGNED NOT NULL,
  `Aset_id_Aset` int(10) UNSIGNED NOT NULL,
  `TabungGas5kg` int(10) UNSIGNED DEFAULT NULL,
  `Kulkas` int(10) UNSIGNED DEFAULT NULL,
  `AC` int(10) UNSIGNED DEFAULT NULL,
  `PemanasAir` int(10) UNSIGNED DEFAULT NULL,
  `TeleponRumah` int(10) UNSIGNED DEFAULT NULL,
  `Televisi` int(10) UNSIGNED DEFAULT NULL,
  `Perhisasan10gr` int(10) UNSIGNED DEFAULT NULL,
  `Komputer_Laptop` int(10) UNSIGNED DEFAULT NULL,
  `Sepeda` int(10) UNSIGNED DEFAULT NULL,
  `SepedaMotor` int(10) UNSIGNED DEFAULT NULL,
  `Mobil` int(10) UNSIGNED DEFAULT NULL,
  `Perahu` int(10) UNSIGNED DEFAULT NULL,
  `MotorTempel` int(10) UNSIGNED DEFAULT NULL,
  `PerahuMotor` int(10) UNSIGNED DEFAULT NULL,
  `Kapal` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asettidakbergerak`
--

CREATE TABLE `asettidakbergerak` (
  `id_AsetTidakBergerak` int(10) UNSIGNED NOT NULL,
  `Aset_id_Aset` int(10) UNSIGNED NOT NULL,
  `Lahan` int(10) UNSIGNED DEFAULT NULL,
  `Rumah` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ekonomi`
--

CREATE TABLE `ekonomi` (
  `id_Ekonomi` int(11) NOT NULL,
  `NIK` int(10) UNSIGNED NOT NULL,
  `Survey_id_Survey` int(10) UNSIGNED NOT NULL,
  `TanggunganPendidikan_id_TanggunganPendidikan` int(10) UNSIGNED NOT NULL,
  `OptionEkonomi_id_OptionEkonomi` int(10) UNSIGNED NOT NULL,
  `id_PengenalanTempat` int(10) UNSIGNED NOT NULL,
  `Nama` varchar(200) DEFAULT NULL,
  `HubKRT` int(10) UNSIGNED DEFAULT NULL,
  `NoKeluarga` int(10) UNSIGNED DEFAULT NULL,
  `HubKepalaKeluarga` int(10) UNSIGNED DEFAULT NULL,
  `JenisKlamin` int(10) UNSIGNED DEFAULT NULL,
  `Umur` int(10) UNSIGNED DEFAULT NULL,
  `StatusPerkawinan` int(10) UNSIGNED DEFAULT NULL,
  `KepemilikanBukuNikah` int(10) UNSIGNED DEFAULT NULL,
  `TercantumDalamKartuKeluarga` int(10) UNSIGNED DEFAULT NULL,
  `KartuIdentitas` int(10) UNSIGNED DEFAULT NULL,
  `StatusKehamilan` int(10) UNSIGNED DEFAULT NULL,
  `JenisCacat` int(10) UNSIGNED DEFAULT NULL,
  `PenyakitKronis` int(10) UNSIGNED DEFAULT NULL,
  `PartisipasiSekolah` int(10) UNSIGNED DEFAULT NULL,
  `JenjangPendidikanTertinggi` int(10) UNSIGNED DEFAULT NULL,
  `KelasTertinggi` int(10) UNSIGNED DEFAULT NULL,
  `IjazahTertinggi` int(10) UNSIGNED DEFAULT NULL,
  `StatusBekerja` int(10) UNSIGNED DEFAULT NULL,
  `LapanganPekerjaan` int(10) UNSIGNED DEFAULT NULL,
  `StatusKedudukanPekerjaan` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenisusaha`
--

CREATE TABLE `jenisusaha` (
  `id_JenisUsaha` int(10) UNSIGNED NOT NULL,
  `Usaha_id_Usaha` int(10) UNSIGNED NOT NULL,
  `id_Ekonomi` int(10) UNSIGNED DEFAULT NULL,
  `JumlahPekerja` int(10) UNSIGNED DEFAULT NULL,
  `TempatUsaha` int(10) UNSIGNED DEFAULT NULL,
  `OmsetUsaha` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_Kecamatan` int(10) UNSIGNED NOT NULL,
  `PengenalanTempat_id_PengenalanTempat` int(10) UNSIGNED NOT NULL,
  `NamaKecamatan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_Kelurahan` int(10) UNSIGNED NOT NULL,
  `Kecamatan_id_Kecamatan` int(10) UNSIGNED NOT NULL,
  `NamaKelurahan` varchar(50) DEFAULT NULL,
  `id_Kecamatan` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `optionaset`
--

CREATE TABLE `optionaset` (
  `id_OptionAset` int(10) UNSIGNED NOT NULL,
  `id_Aset` int(10) UNSIGNED DEFAULT NULL,
  `Label` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `optionekonomi`
--

CREATE TABLE `optionekonomi` (
  `id_OptionEkonomi` int(10) UNSIGNED NOT NULL,
  `id_Ekonomi` int(10) UNSIGNED DEFAULT NULL,
  `Label` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `optionperumahan`
--

CREATE TABLE `optionperumahan` (
  `id_OptionPerumahan` int(10) UNSIGNED NOT NULL,
  `Perumahan_id_Perumahan` int(10) UNSIGNED NOT NULL,
  `id_Perumahan` int(10) UNSIGNED DEFAULT NULL,
  `Label` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengenalantempat`
--

CREATE TABLE `pengenalantempat` (
  `id_PengenalanTempat` int(10) UNSIGNED NOT NULL,
  `id_Kecamatan` int(10) UNSIGNED DEFAULT NULL,
  `NamaSLS` varchar(200) DEFAULT NULL,
  `Alamat` varchar(200) DEFAULT NULL,
  `NamaKRT` varchar(200) DEFAULT NULL,
  `JumlahART` int(10) UNSIGNED DEFAULT NULL,
  `JumlahKeluarga` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_Pengguna` int(10) UNSIGNED NOT NULL,
  `KataKunci` varchar(100) NOT NULL,
  `Survey_id_Survey` int(10) UNSIGNED NOT NULL,
  `NamaPengguna` varchar(30) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `perumahan`
--

CREATE TABLE `perumahan` (
  `id_Perumahan` int(10) UNSIGNED NOT NULL,
  `StatusBangunan` int(10) UNSIGNED DEFAULT NULL,
  `StatusLahan` int(10) UNSIGNED DEFAULT NULL,
  `LuasLantai` int(10) UNSIGNED DEFAULT NULL,
  `JenisLantai` int(10) UNSIGNED DEFAULT NULL,
  `JenisDinding` int(10) UNSIGNED DEFAULT NULL,
  `KondisiDinding` int(10) UNSIGNED DEFAULT NULL,
  `JenisAtap` int(10) UNSIGNED DEFAULT NULL,
  `KondisiAtap` int(10) UNSIGNED DEFAULT NULL,
  `JmlKamarTidur` int(10) UNSIGNED DEFAULT NULL,
  `SumberAirMinum` int(10) UNSIGNED DEFAULT NULL,
  `MemperolehAirMinum` int(10) UNSIGNED DEFAULT NULL,
  `PeneranganUtama` int(10) UNSIGNED DEFAULT NULL,
  `DayaPenerangan` int(10) UNSIGNED DEFAULT NULL,
  `BahanBakarMemasak` int(10) UNSIGNED DEFAULT NULL,
  `FasilitasBAB` int(10) UNSIGNED DEFAULT NULL,
  `JenisKloset` int(10) UNSIGNED DEFAULT NULL,
  `TempatPembuanganTinja` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_Petugas` int(10) UNSIGNED NOT NULL,
  `Tgl_Pencacahan` date DEFAULT NULL,
  `NamaPencacahan` varchar(200) DEFAULT NULL,
  `TglPemeriksa` date DEFAULT NULL,
  `NamaPemeriksa` varchar(200) DEFAULT NULL,
  `HasilPencacahan` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id_Program` int(10) UNSIGNED NOT NULL,
  `Aset_id_Aset` int(10) UNSIGNED NOT NULL,
  `KKS_KPS` int(10) UNSIGNED DEFAULT NULL,
  `KIP_BSM` int(10) UNSIGNED DEFAULT NULL,
  `KIS_BPJS` int(10) UNSIGNED DEFAULT NULL,
  `BPJS_Mandiri` int(10) UNSIGNED DEFAULT NULL,
  `Jamsostek` int(10) UNSIGNED DEFAULT NULL,
  `AsuransiKesehatan` int(10) UNSIGNED DEFAULT NULL,
  `RASKIN` int(10) UNSIGNED DEFAULT NULL,
  `KUR` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE `survey` (
  `id_Survey` int(10) UNSIGNED NOT NULL,
  `Perumahan_id_Perumahan` int(10) UNSIGNED NOT NULL,
  `Aset_id_Aset` int(10) UNSIGNED NOT NULL,
  `Petugas_id_Petugas` int(10) UNSIGNED NOT NULL,
  `PengenalanTempat_id_PengenalanTempat` int(10) UNSIGNED NOT NULL,
  `id_Pengguna` int(10) UNSIGNED DEFAULT NULL,
  `id_PengenalanTempat` int(10) UNSIGNED DEFAULT NULL,
  `id_Petugas` int(10) UNSIGNED DEFAULT NULL,
  `id_Perumahan` int(10) UNSIGNED DEFAULT NULL,
  `id_Ekonomi` int(10) UNSIGNED DEFAULT NULL,
  `id_Aset` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tanggunganpendidikan`
--

CREATE TABLE `tanggunganpendidikan` (
  `id_TanggunganPendidikan` int(10) UNSIGNED NOT NULL,
  `id_Ekonomi` int(10) UNSIGNED NOT NULL,
  `id_PengenalanTempat` int(10) UNSIGNED DEFAULT NULL,
  `NamaSekolah` varchar(200) DEFAULT NULL,
  `NISN_NoKTM` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ternak`
--

CREATE TABLE `ternak` (
  `id_Ternak` int(10) UNSIGNED NOT NULL,
  `Aset_id_Aset` int(10) UNSIGNED NOT NULL,
  `Sapi` int(10) UNSIGNED DEFAULT NULL,
  `Kerbau` int(10) UNSIGNED DEFAULT NULL,
  `Kuda` int(10) UNSIGNED DEFAULT NULL,
  `Babi` int(10) UNSIGNED DEFAULT NULL,
  `Kambing_Domba` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usaha`
--

CREATE TABLE `usaha` (
  `id_Usaha` int(10) UNSIGNED NOT NULL,
  `Aset_id_Aset` int(10) UNSIGNED NOT NULL,
  `id_JenisUsaha` int(10) UNSIGNED DEFAULT NULL,
  `MemilikiUsaha` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aset`
--
ALTER TABLE `aset`
  ADD PRIMARY KEY (`id_Aset`),
  ADD KEY `Aset_FKIndex1` (`OptionAset_id_OptionAset`);

--
-- Indexes for table `asetbergerak`
--
ALTER TABLE `asetbergerak`
  ADD PRIMARY KEY (`id_AsetBergerak`),
  ADD KEY `AsetBergerak_FKIndex1` (`Aset_id_Aset`);

--
-- Indexes for table `asettidakbergerak`
--
ALTER TABLE `asettidakbergerak`
  ADD PRIMARY KEY (`id_AsetTidakBergerak`),
  ADD KEY `AsetTidakBergerak_FKIndex1` (`Aset_id_Aset`);

--
-- Indexes for table `ekonomi`
--
ALTER TABLE `ekonomi`
  ADD PRIMARY KEY (`id_Ekonomi`,`NIK`),
  ADD KEY `Ekonomi_FKIndex1` (`OptionEkonomi_id_OptionEkonomi`),
  ADD KEY `Ekonomi_FKIndex2` (`TanggunganPendidikan_id_TanggunganPendidikan`),
  ADD KEY `Ekonomi_FKIndex3` (`Survey_id_Survey`);

--
-- Indexes for table `jenisusaha`
--
ALTER TABLE `jenisusaha`
  ADD PRIMARY KEY (`id_JenisUsaha`),
  ADD KEY `JenisUsaha_FKIndex1` (`Usaha_id_Usaha`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_Kecamatan`),
  ADD KEY `Kecamatan_FKIndex1` (`PengenalanTempat_id_PengenalanTempat`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_Kelurahan`),
  ADD KEY `Kelurahan_FKIndex1` (`Kecamatan_id_Kecamatan`);

--
-- Indexes for table `optionaset`
--
ALTER TABLE `optionaset`
  ADD PRIMARY KEY (`id_OptionAset`);

--
-- Indexes for table `optionekonomi`
--
ALTER TABLE `optionekonomi`
  ADD PRIMARY KEY (`id_OptionEkonomi`);

--
-- Indexes for table `optionperumahan`
--
ALTER TABLE `optionperumahan`
  ADD PRIMARY KEY (`id_OptionPerumahan`),
  ADD KEY `OptionPerumahan_FKIndex1` (`Perumahan_id_Perumahan`);

--
-- Indexes for table `pengenalantempat`
--
ALTER TABLE `pengenalantempat`
  ADD PRIMARY KEY (`id_PengenalanTempat`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_Pengguna`,`KataKunci`),
  ADD KEY `Pengguna_FKIndex1` (`Survey_id_Survey`);

--
-- Indexes for table `perumahan`
--
ALTER TABLE `perumahan`
  ADD PRIMARY KEY (`id_Perumahan`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_Petugas`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id_Program`),
  ADD KEY `Program_FKIndex1` (`Aset_id_Aset`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id_Survey`),
  ADD KEY `Survey_FKIndex1` (`PengenalanTempat_id_PengenalanTempat`),
  ADD KEY `Survey_FKIndex2` (`Petugas_id_Petugas`),
  ADD KEY `Survey_FKIndex3` (`Aset_id_Aset`),
  ADD KEY `Survey_FKIndex4` (`Perumahan_id_Perumahan`);

--
-- Indexes for table `tanggunganpendidikan`
--
ALTER TABLE `tanggunganpendidikan`
  ADD PRIMARY KEY (`id_TanggunganPendidikan`);

--
-- Indexes for table `ternak`
--
ALTER TABLE `ternak`
  ADD PRIMARY KEY (`id_Ternak`),
  ADD KEY `Ternak_FKIndex1` (`Aset_id_Aset`);

--
-- Indexes for table `usaha`
--
ALTER TABLE `usaha`
  ADD PRIMARY KEY (`id_Usaha`),
  ADD KEY `Usaha_FKIndex1` (`Aset_id_Aset`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aset`
--
ALTER TABLE `aset`
  MODIFY `id_Aset` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asetbergerak`
--
ALTER TABLE `asetbergerak`
  MODIFY `id_AsetBergerak` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asettidakbergerak`
--
ALTER TABLE `asettidakbergerak`
  MODIFY `id_AsetTidakBergerak` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ekonomi`
--
ALTER TABLE `ekonomi`
  MODIFY `id_Ekonomi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenisusaha`
--
ALTER TABLE `jenisusaha`
  MODIFY `id_JenisUsaha` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_Kecamatan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id_Kelurahan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `optionaset`
--
ALTER TABLE `optionaset`
  MODIFY `id_OptionAset` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `optionekonomi`
--
ALTER TABLE `optionekonomi`
  MODIFY `id_OptionEkonomi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `optionperumahan`
--
ALTER TABLE `optionperumahan`
  MODIFY `id_OptionPerumahan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengenalantempat`
--
ALTER TABLE `pengenalantempat`
  MODIFY `id_PengenalanTempat` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_Pengguna` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `perumahan`
--
ALTER TABLE `perumahan`
  MODIFY `id_Perumahan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_Petugas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id_Program` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `id_Survey` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tanggunganpendidikan`
--
ALTER TABLE `tanggunganpendidikan`
  MODIFY `id_TanggunganPendidikan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ternak`
--
ALTER TABLE `ternak`
  MODIFY `id_Ternak` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usaha`
--
ALTER TABLE `usaha`
  MODIFY `id_Usaha` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
