<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>

    <div>
    <header class="main-header">

      <!-- Logo -->
      <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>BAN</b>SOS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>BANSOS (Bantuan Sosial)</b></span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->

            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">Ridwan Fajar Widodo</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    Ridwan Fajar Widodo
                    <small>Dinas Sosial Kota Malang</small>
                  </p>
                </li>
                <!-- Menu Body -->

                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php echo base_url(); ?>login" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Ridwan Fajar Widodo</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i>Dinas Sosial Kota Malang</a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">menu</li>
          <!-- Optionally, you can add icons to the links -->
          <li id="/"><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <!-- <li><a href="<?php echo base_url('index.php/formulir'); ?>"><i class="fa fa-edit"></i> <span>Formulir</span></a></li>
           -->
          <li class="treeview">
            <a href="<?php echo base_url(); ?>"><i class="fa fa-laptop"></i> <span>Formulir</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('index.php/formulir'); ?>"><i class="fa fa-circle-o"></i>Sendirian</a></li>
              <li><a href="<?php echo base_url('index.php/formulirkeluarga'); ?>"><i class="fa fa-circle-o"></i>Keluargaan</a></li>
            </ul>
          </li>
          <li><a href="<?php echo base_url(); ?>"><i class="fa fa-table"></i> <span>Data</span></a></li>
          <li class="treeview">
            <a href="<?php echo base_url(); ?>"><i class="fa fa-laptop"></i> <span>Perhitungan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>"><i class="fa fa-circle-o"></i>PROMETHEE I</a></li>
              <li><a href="<?php echo base_url(); ?>"><i class="fa fa-circle-o"></i>PROMETHEE II</a></li>
            </ul>
          </li>
          <li><a href="<?php echo base_url(); ?>"><i class="fa fa-link"></i> <span>Voting</span></a></li>
          <li><a href="<?php echo base_url(); ?>"><i class="fa fa-link"></i> <span>About me</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
      </section>
      <!-- /.sidebar -->
    </aside>
    </div>

    <script type="text/javascript">
      var pathname = window.location.pathname;
      // alert(pathname);
 
      pathname = pathname.substring(11);

      $(".sidebar-menu li").removeClass("active");
      $("#"+pathname).addClass("active");


    </script>