<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BANSOS (Bantuan Sosial)</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
        <link rel="stylesheet" href="assets/dist/css/skins/skin-blue.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?php $this->load->view('header');?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <!-- 4kotak diatas  -->
        <div class="row"> <!--Infografis Kemiskinan--> 
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>150</h3>
                <p>Jumlah kemiskinan <br> di kota Malang</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>Jumlah penerima <br> bantuan kemiskinan</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>44</h3>

                <p>Jumlah anggaran <br> yang dikeluarkan</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3>65</h3>

                <p>Jumlah petugas<br>survey</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <div class="row"><!--Grafik Catatan Kemiskinan-->
          <div class="col-md-12 ">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Laporan Bulanan</h3>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-center">
                        <strong>Laporan bantuan kemiskinan Tahun 2016 s/d 2018</strong>
                      </p>
                      <div class="chart"> 
                        <canvas id="salesChart" style="height: 180px;"></canvas>
                      </div>
                      <!-- /.chart-responsive -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <div class="row"> <!--Donut Chart PMKS-->
          <div class="col-md-8">
           <div class="box box-default">  
            <div class="box-header with-border">
              <h3 class="box-title">Penyandang Masalah Kesejahteraan Sosial (PMKS)<br><br>
                <small>Jumlah Penerima bamyiam kemiskinan : 1256</small>
              </h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="150"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o text-red"></i> Anak berkebutuhan khusus</li>
                    <li><i class="fa fa-circle-o text-green"></i> Pengemis</li>
                    <li><i class="fa fa-circle-o text-yellow"></i> Anak jalanan</li>
                    <li><i class="fa fa-circle-o text-aqua"></i> Gelandangan</li>
                    <li><i class="fa fa-circle-o text-light-blue"></i> Anak balita terlantar</li>
                    <li><i class="fa fa-circle-o text-gray"></i> Anak yang bermasalah dengan hukum</li>
                  </ul>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-primary"> 
            <div class="box-header with-border">
              <h3 class="box-title">Informasi Penyebab Kemiskinan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <li class="item">
                  <div class="product-img">
                    <img src="assets/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <a href="javascript:void(0)" class="product-title">Pendidikan Rendah 
                      <span class="label label-warning pull-right">62%</span></a>
                      <span class="product-description">Berpendidikan maksimal hanya sampai <br> kelas 2 SMP atau sederajat
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product-img">
                      <img src="assets/dist/img/default-50x50.gif" alt="Product Image">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">Pengangguran 
                        <span class="label label-info pull-right">19%</span></a>
                        <span class="product-description">
                          Tidak memiliki pekerjaan sehingga <br> tidak mampu  memenuhi kebutuhan <br>pokok sehari-harinya
                        </span>
                      </div>
                    </li>
                    <!-- /.item -->
                    <li class="item">
                      <div class="product-img">
                        <img src="assets/dist/img/default-50x50.gif" alt="Product Image">
                      </div>
                      <div class="product-info">
                        <a href="javascript:void(0)" class="product-title">Penyakit<span
                          class="label label-danger pull-right">13%</span></a>
                          <span class="product-description">
                            Mengalami penyakit kronis dan <br> tidak mampu berobat ke rumah sakit <br> kecuali yang disubsidi oleh pemerintah </span>
                          </div>
                        </li>
                        <!-- /.item -->
                        <li class="item">
                          <div class="product-img">
                            <img src="assets/dist/img/default-50x50.gif" alt="Product Image">
                          </div>
                          <div class="product-info">
                            <a href="javascript:void(0)" class="product-title">Lainnya
                              <span class="label label-success pull-right">6%</span></a>
                              <span class="product-description">
                                Penyebab tidak diketahui secara pasti
                              </span>
                            </div>
                          </li>
                          <!-- /.item -->
                        </ul>
                      </div>
                      <!-- /.box-footer -->
                    </div>
                    <!-- /.content -->
                  </div>
                </div> 

                <button type="submit" class="btn btn-success float-right" id="submit" style="display: none ; float: right;">Submit</button>

                <!-- grafik history kematian-->
              </div>
              <footer class="main-footer">
                <!-- Default to the left -->
                <strong>Copyright &copy; 2019 <a href="#">FILKOM</a>.</strong> All rights reserved.
              </footer>
            </div>
            <!-- /.content-wrapper -->
          </section>
          <!-- Main Footer -->
          

          <!-- Control Sidebar -->
          <aside class="control-sidebar control-sidebar-dark">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
              <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
              <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
            </ul>
            <!-- Tab panes -->
          </aside>
          <!-- /.control-sidebar -->
                  <!-- Add the sidebar's background. This div must be placed
                    immediately after the control sidebar -->
                    <div class="control-sidebar-bg"></div>
                  </div>
                  <!-- ./wrapper -->

                  <!-- REQUIRED JS SCRIPTS -->

                  <!-- jQuery 3 -->
                  <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
                  <!-- Bootstrap 3.3.7 -->
                  <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
                  <!-- FastClick -->
                  <script src="assets/bower_components/fastclick/lib/fastclick.js"></script>
                  <!-- AdminLTE App -->
                  <script src="assets/dist/js/adminlte.min.js"></script>
                  <!-- Sparkline -->
                  <script src="assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
                  <!-- jvectormap  -->
                  <script src="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
                  <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
                  <!-- SlimScroll -->
                  <script src="assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
                  <!-- ChartJS -->
                  <script src="assets/bower_components/chart.js/Chart.js"></script>
                  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
                  <script src="assets/dist/js/pages/dashboard2.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
   </body>
   </html>