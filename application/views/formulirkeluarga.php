<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BANSOS (Bantuan Sosial)</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/AdminLTE.min.css">
 <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/skins/skin-blue.min.css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <!--   select2 -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/select2/dist/css/select2.min.css">
   <!-- Google Font -->
   <link rel="stylesheet"
   href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <style type="text/css">
     .item img {width: 100%; height: auto}
   </style>
 </head>

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?php $this->load->view('header');?>

    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"> <!--Tampilan menu-->
    <h1>
      Formulir Penanganan Kemiskinan
      <small>Dinas Sosial Kota Malang</small>
    </h1>
  </section>
  
  <section class="content container-fluid">
    <!-- Awal Formulir Penanganan Kemiskinan -->
    <form role="form" method="post" action="<?php echo base_url('home/submitFormulir'); ?>">
    <div id="Petugas" style="display: none"> <!-- Form keterangan petugas dan responden -->
        <div class="box box-primary"> 
          <div class="box-header with-border">
            <h3 class="box-title">Keterangan Petugas dan Responden</h3>
          </div>

          <div class="box-body">  <!-- form isian petugas -->
            <div class="form-group">
              <label>Tanggal Pencacahan</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="Tgl_Pencacahan" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="Tgl_Pencacahan">
              </div>
            </div>

            <div class="form-group">
              <label>Nama Pencacah</label>
              <input type="text" class="form-control" id="NamaPencacahan" name="NamaPencacahan" placeholder="Masukan Nama">
            </div>
          </div>
        </div>
      </div>
      <div class="box box-primary"> <!-- Form pengenalan tempat -->
          <div class="box-header with-border">
            <h3 class="box-title">Pengenalan Tempat</h3>
          </div>

          <div class="box-body" >   <!-- form pengenalan tempat -->
            <div class="form-group">
              <label>Kecamatan</label>
              <select class="form-control select2" style="width: 100%;"id="NamaKecamatan" name="Kecamatan">
                <option selected="selected" disabled> Masukan kecamatan</option>
                <option>Kedungkandang</option>
                <option>Klojen</option>
                <option>Lowokwaru</option>
                <option>Sukun</option>
              </select>
            </div>

            <div class="form-group" >
              <label>Kelurahan</label>
              <select class="form-control select2" style="width: 100%;" id="NamaKelurahan" name="Kelurahan">
                <option selected="selected" disabled>Masukan Kelurahan</option>
                <option>Arjosari</option>
                <option>Arjowinangun</option>
                <option>Bakalankrajan</option>
                <option>Balearjosari</option>
                <option>Bandulan</option>
                <option>Bandungrejosari</option>
                <option>Bareng</option>
                <option>Blimbing</option>
                <option>Bumiayu</option>
                <option>Bunulrejo</option>
                <option>Buring</option>
                <option>Cemorokandang</option>
                <option>Ciptomulyo</option>
                <option>Dinoyo</option>
                <option>Gadang</option>
                <option>Gadingasri</option>
                <option>Jatimulyo</option>
                <option>Jodipan</option>
                <option>Karangbesuki</option>
                <option>Kasin</option>
                <option>Kauman</option>
                <option>Kebonsari</option>
                <option>Kedungkandang</option>
                <option>Kesatrian</option>
                <option>Ketawanggede</option>
                <option>Kiduldalem</option>
                <option>Klojen</option>
                <option>Kotalama</option>
                <option>Lesanpuro</option>
                <option>Lowokwaru</option>
                <option>Madyopuro</option>
                <option>Mergosono</option>
                <option>Merjosari</option>
                <option>Mojolangu</option>
                <option>Mulyorejo</option>
                <option>Oro-Oro Dowo</option>
                <option>Pandanwangi</option>
                <option>Penanggungan</option>
                <option>Pisangcandi</option>
                <option>Polehan</option>
                <option>Polowijen</option>
                <option>Purwantoro</option>
                <option>Purwodadi</option>
                <option>Rampal Celaket</option>
                <option>Samaan</option>
                <option>Sawojajar</option>
                <option>Sukoharjo</option>
                <option>Sukun</option>
                <option>Sumbersari</option>
                <option>Tanjungrejo</option>
                <option>Tasikmadu</option>
                <option>Tlogomas</option>
                <option>Tlogowaru</option>
                <option>Tulusrejo</option>
                <option>Tunggulwulung</option>
                <option>Tunjungsekar</option>
                <option>Wonokoyo</option>
              </select>
            </div>

            <div class="form-group">
              <label>Nama SLS (RT)</label>
              <input type="text" class="form-control" id="NamaSLS" name="RT" placeholder="RT">
            </div>
            <div class="form-group">
              <label>Nama SLS (RW)</label>
              <input type="text" class="form-control" id="NamaSLS" name="RW" placeholder="RW">
            </div>
            <div class="form-group">
              <label>Alamat tempat tinggal</label>
              <input type="text" class="form-control" id="Alamat" name="Alamat" placeholder="Masukan Alamat">
            </div>         
            <div class="form-group">
              <label>NamaKRT</label>
              <input type="text" class="form-control" id="NamaKRT" name="NamaKRT" placeholder="Nama Kepala Rumah Tangga">
            </div>
            <div class="form-group">
              <label>JumlahART</label>
              <input type="number" class="form-control" id="JumlahART" name="JumlahART" placeholder="Jumlah Anggota Rumah Tangga">
            </div>
            <div class="form-group">
              <label>Jumlah Keluarga</label>
              <input type="number" class="form-control" id="JumlahKeluarga" name="JumlahKeluarga" placeholder="Masukan Jumlah Keluarga dalam satu rumah">
            </div>
         
      

      <!-- <div id="Petugas" style="display: none"> 
        <div class="box box-primary"> 
          <div class="box-header with-border">
            <h3 class="box-title">Keterangan Petugas dan Responden</h3>
          </div> -->

            <div class="form-group">
              <label>Tanggal Pencacahan</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="Tgl_Pencacahan" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="Tgl_Pencacahan">
              </div>
            </div>

            <div class="form-group">
              <label>Nama Pencacah</label>
              <input type="text" class="form-control" id="NamaPencacahan" name="NamaPencacahan" placeholder="Masukan Nama">
            </div>

            <div class="form-group">
              <label >Tanggal Pemeriksa</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="TglPemeriksa" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="TglPemeriksa">
              </div>
            </div>

            <div class="form-group">
              <label>Nama Pemeriksa</label>
              <input type="text" class="form-control" id="NamaPemeriksa" name="NamaPemeriksa" placeholder="Masukan Nama Pemeriksa">
            </div>
            
            <div class="form-group">
              <label>Hasil Pencacahan Rumah Tangga</label>
              <select class="form-control" style="width: 100%;"id="HasilPencacahan" name="HasilPencacahan">
                <option selected="selected" disabled> Hasil pencacahan</option>
                <option>Selesai dicacah</option>
                <option>Rumah tangga tidak ditemukan</option>
                <option>Rumah tangga pindah/bangunan sensus  sudah tidak ada</option>
                <option>Bagian dari rumah tangga di dokumen PBDT2015</option>
              </select>
            </div>
         </div>

         
      <!-- Form keterangan perumahan -->
      <!-- <div id="Perumahan" style="display: none"> 
        <div class="box box-primary"> 
          <div class="box-header with-border">
            <h3 class="box-title">Keterangan Perumahan</h3>
          </div> -->

          <!-- form isian perumahan -->
          <div class="box-body">  
            <div class="form-group">
              <label>Status penguasaan bangunan tempat tinggal yang ditempati:</label>
              <select class="form-control" style="width: 100%;"id="StatusBangunan" name="StatusBangunan">
                <option selected="selected" disabled>Status penguasaan bangunan</option>
                <option>1. Milik sendiri</option>
                <option>2. Kontrak/sewa</option>
                <option>3. Bebas sewa</option>
                <option>4. Dinas</option>
                <option>5. Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Status lahan tempat tinggal yang ditempati</label>
              <select class="form-control" style="width: 100%;"id="StatusLahan" name="StatusLahan">
                <option selected="selected" disabled>Status lahan</option>
                <option>1. Milik sendiri</option>
                <option>2. Mirik orang lain</option>
                <option>3. Tanah negara</option>
                <option>4. Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Luas lantai</label>
              <input type="number" class="form-control" id="LuasLantai" name="LuasLantai" placeholder="Masukkan luas lantai">
            </div>

            <div class="form-group">
              <label>Jenis lantai terluas</label>
              <select class="form-control" style="width: 100%;"id="JenisLantai" name="JenisLantai">
                <option selected="selected" disabled>Jenis lantai</option>
                <option>1. Marmer/granit</option>
                <option>2. Keramik</option>
                <option>3. Paret/vinil/permadani</option>
                <option>4. Ubin/tegel/teraso</option>
                <option>5. Kayu/papan kualitas tinggi</option>
                <option>6. Semen/bata merah</option>
                <option>7. Bambu</option>
                <option>8. Kayu/papan kualitas rendah</option>
                <option>9. Tanah</option>
                <option>10.Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Jenis dinding terluas</label>
              <select class="form-control" style="width: 100%;"id="JenisDinding" name="JenisDinding">
                <option selected="selected" disabled>Jenis dinding</option>
                <option>1. Tembok</option>
                <option>2. Plesteran anyaman bambu/kawat</option>
                <option>3. Kayu</option>
                <option>4. Anyaman Bambu</option>
                <option>5. Batang kayu</option>
                <option>6. Bambu</option>
                <option>7. Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Kondisi dinding</label>
              <select class="form-control" style="width: 100%;"id="KondisiDinding" name="KondisiDinding">
                <option selected="selected" disabled>Kondisi dinding</option>
                <option>1. Bagus/kualitas tinggi</option>
                <option>2. Jelek/kualitas rendah</option>
              </select>
            </div>

            <div class="form-group">
              <label>Jenis atap terluas</label>
              <select class="form-control" style="width: 100%;"id="JenisAtap" name="JenisAtap">
                <option selected="selected" disabled>Jenis atap</option>
                <option>1. Beton/geteng beton</option>
                <option>2. Genteng keramik</option>
                <option>3. Genteng metal</option>
                <option>4. Genteng tanah liat</option>
                <option>5. Asbes</option>
                <option>6. Seng</option>
                <option>7. Sirap</option>
                <option>8. Bambu</option>
                <option>9. Jerami/ijuk/daun daunan/rambia</option>
                <option>10.Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Kondisi atap</label>
              <select class="form-control" style="width: 100%;"id="KondisiAtap" name="KondisiAtap">
                <option selected="selected" disabled>Kondisi atap</option>
                <option>1. Bagus/kualitas tinggi</option>
                <option>2. Jelek/kualitas redah</option>
              </select>
            </div>

            <div class="form-group">
              <label>Jumlah kamar tidur</label>
              <input type="number" class="form-control" id="JmlKamarTidur" name="JmlKamarTidur" placeholder="Masukkan jumlah kamar tidur">
            </div>

            <div class="form-group">
              <label>Sumber air minum</label>
              <select class="form-control" style="width: 100%;"id="SumberAirMinum" name="SumberAirMinum">
                <option selected="selected" disabled>Sumber air minum</option>
                <option>1. Air kemasan bermerk</option>
                <option>2. Air isi ulang</option>
                <option>3. Leding meteran</option>
                <option>4. Leding eceran</option>
                <option>5. Sumur bor/pompa</option>
                <option>6. Sumur terlindungi</option>
                <option>7. Sumur tak terlingdungi</option>
                <option>8. Mata air terlidungi</option>
                <option>9. Mata air tak terlindungi</option>
                <option>10.Air sungai/danau/waduk</option>
                <option>11.Air hujan</option>
                <option>12.Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Cara memperoleh air minum</label>
              <select class="form-control" style="width: 100%;"id="MemperolehAirMinum" name="MemperolehAirMinum">
                <option selected="selected" disabled>Cara memperoleh</option>
                <option>1. Membeli eceran</option>
                <option>2. Langganan</option>
                <option>3. Tidak berlangganan</option>
              </select>
            </div>

            <div class="form-group">
              <label>Sumber penerangan utama</label>
              <select class="form-control" style="width: 100%;"id="PeneranganUtama" name="PeneranganUtama">
                <option selected="selected" disabled>Sumber penerangan</option>
                <option>1. Listrik PLN</option>
                <option>2. Listrik non PLN</option>
                <option>3. Bukan listrik</option>
              </select>
            </div>

            <div class="form-group">
              <label>Daya penerangan</label>
              <select class="form-control" style="width: 100%;"id="DayaPenerangan" name="DayaPenerangan">
                <option selected="selected" disabled>Daya</option>
                <option>1. 450 watt</option>
                <option>2. 900 watt</option>
                <option>3. 1.300 watt</option>
                <option>4. 2.200 watt</option>
                <option>5. > 2.200 watt</option>
                <option>6. Tanpa meteran</option>
              </select>
            </div>
            
            <div class="form-group">
              <label>Bahan bakar/energi utama untuk memasak</label>
              <select class="form-control" style="width: 100%;"id="BahanBakarMasak" name="BahanBakarMasak">
                <option selected="selected" disabled>Bahan bakar</option>
                <option>1. Listrik</option>
                <option>2. Gas > 3 kg</option>
                <option>3. Gas 3 kg</option>
                <option>4. Gas kota/biogas</option>
                <option>5. Minyak tanah</option>
                <option>6. Briket</option>
                <option>7. Arang</option>
                <option>8. Kayu bakar</option>
                <option>9. Tidak memasak di rumah</option>
              </select>
            </div>

            <div class="form-group">
              <label>Penggunaan fasilitas tempat buang air besar</label>
              <select class="form-control" style="width: 100%;"id="FasilitasBAB" name="FasilitasBAB">
                <option selected="selected" disabled>Fasilitas BAB</option>
                <option>1. Sendiri</option>
                <option>2. Bersama</option>
                <option>3. Umum</option>
                <option>4. Tidak ada</option>
              </select>
            </div>

            <div class="form-group">
              <label>Jenis kloset</label>
              <select class="form-control" style="width: 100%;"id="JenisKloset" name="JenisKloset">
                <option selected="selected" disabled>Jenis kloset</option>
                <option>1. Leher angsa</option>
                <option>2. Plengsengan</option>
                <option>3. Cemplung/cubluk</option>
                <option>4. Tidak pakai</option>
              </select>
            </div>

            <div class="form-group">
              <label>Tempat pembuangan akhir tinja</label>
              <select class="form-control" style="width: 100%;"id="TempatPembuanganTinja" name="TempatPembuanganTinja">
                <option selected="selected" disabled>Tempat pembuangan akhir</option>
                <option>1. Tangki</option>
                <option>2. SPAL</option>
                <option>3. Lubang tanah</option>
                <option>4. Kolam/sawah/sungai/danau/laut</option>
                <option>5. Pantai/tanah lapang/kebun</option>
                <option>6. Lainnya</option>
              </select>
            </div>
          </div>
      
      <!-- Form keterangan sosial ekonomi anggota rumah tangga -->
      <!-- <div id="Ekonomi" style="display: none" > 
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Keterangan Sosial Ekonomi Anggota Rumah Tangga</h3>
          </div> -->
          
          <!-- form ekonomi -->
          <div class="box-body">  
            <div class="form-group">
              <label>Nama</label><br>
              <input type="text" class="form-control" id="Nama" name="Nama" placeholder="Masukan nama anggota rumah tangga">
            </div>

            <div class="form-group">
              <label>NIK</label>
              <input type="text" class="form-control" id="NIK" name="NIK" placeholder="Masukan NIK anggota rumah tangga">
            </div>

            <div class="form-group">
              <label>Hubungan dengan kepala rumah tangga</label>
              <select class="form-control" style="width: 100%;"id="HubKRT" name="HubKRT">
                <option selected="selected" disabled> Masukan hubungan kepala rumah tangga</option>
                <option>1. Kepala rumah tangga</option>
                <option>2. Istri/suami</option>
                <option>3. Anak</option>
                <option>4. Menantu</option>
                <option>5. Cucu</option>
                <option>6. Orang tua/mertua</option>
                <option>7. Pumbantu Ruta</option>
                <option>8. Lainnya</option>
              </select>
            </div>
            
            <div class="form-group">
              <label>Nomor urut keluarga</label>
              <input type="text" class="form-control" id="NoKeluarga" name="NoKeluarga" placeholder="Masukan nomor urut keluarga">
            </div>

            <div class="form-group">
              <label>Hubungan dengan kepala keluarga</label>
              <select class="form-control" style="width: 100%;"id="HubKepalaKeluarga" name="HubKepalaKeluarga">
                <option selected="selected" disabled>Masukan hubungan kepala keluarga</option>
                <option>1. Kepala keluarga</option>
                <option>2. Istri/suami</option>
                <option>3. Anak</option>
                <option>4. Menantu</option>
                <option>5. Cucu</option>
                <option>6. Orang tua/mertua</option>
                <option>7. Pumbantu Ruta</option>
                <option>8. Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Jenis kelamin</label>
              <select class="form-control" style="width: 100%;"id="JenisKlamin" name="JenisKlamin">
                <option selected="selected" disabled=> Masukkan jenis kelamin</option>
                <option>1. Laki-Laki</option>
                <option>2. Perempuan</option>
              </select>
            </div>

            <div class="form-group">
              <label>Umur</label>
              <input type="text" class="form-control" id="Umur" name="Umur" placeholder="Masukan umur anda">
            </div>

            <div class="form-group">
              <label>Status perkawinan</label>
              <select class="form-control" style="width: 100%;"id="StatusPerkawinan" name="StatusPerkawinan">
                <option selected="selected" disabled>Masukan status perkawinan</option>
                <option>1. Belum kawin</option>
                <option>2. Kawin/nikah</option>
                <option>3. Cerai hidup</option>
                <option>4. Cerai mati</option>
              </select>
            </div>

            <div class="form-group">
              <label>Kepemiliki akta/buku nikah atau akta cerai</label>
              <select class="form-control" style="width: 100%;"id="KepemilikanBukuNikah" name="KepemilikanBukuNikah">
                <option selected="selected" disabled>Masukan kepemilikan buku nikah</option>
                <option>0. Tidak</option>
                <option>1. Ya, dapat ditunjukkan</option>
                <option>2. Ya, tidak dapat ditunjukkan</option>
              </select>
            </div>

            <div class="form-group">
              <label>Tercantum dalam Kartu Keluarga (KK) di rumah tangga ini</label>
              <select class="form-control" style="width: 100%;"id="TercantumDalamKertuKeluarga" name="TercantumDalamKertuKeluarga">
                <option selected="selected"></option>
                <option>1. Ya/option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Kepemilikan kartu keluarga</label><br>
              <input type="checkbox" value="TidakMemilikiAkta" name="KartuIdentitas" checked> 0. Tidak memiliki akta <br>
              <input type="checkbox" value="AktaKelahiran" name="KartuIdentitas" checked> 1. Akta kelahiran <br>
              <input type="checkbox" value="KartuPelajar" name="KartuIdentitas" checked> 2. Kartu pelajar<br>
              <input type="checkbox" value="KTP" name="KartuIdentitasu" checked> 4. KTP <br>
              <input type="checkbox" value="SIM" name="KartuIdentitasu" checked> 8. SIM <br>
            </div> 

      <!-- <div id="Aset" style="display: none">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Kepemilikan aset dan keikutsertaan program</h3>
          </div> -->

          <div class="box-body">  <!-- form Rumah tangga memiliki sendiri aset bergerak -->
            <h5 class="box-title">Rumah tangga memiliki sendiri aset bergerak</h5>
            <div class="form-group">
              <label>Tabung gas 5,5 kg atau lebih</label>
              <select class="form-control" style="width: 100%;"id="TabungGas5kg" name="TabungGas5kg">
                <option selected="selected" disabled>Memiliki tabung gas 5,5 kg atau lebih</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Lemari es/kulkas</label>
              <select class="form-control" style="width: 100%;"id="Kulkas" name="Kulkas">
                <option selected="selected" disabled>Memiliki lemari es/kulkas</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Pemanas air/waterheater</label>
              <select class="form-control" style="width: 100%;"id="PemanasAir" name="PemanasAir">
                <option selected="selected" disabled>Memiliki pemanas air</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Telepon rumah (PSTN)</label>
              <select class="form-control" style="width: 100%;"id="TeleponRumah" name="TeleponRumah">
                <option selected="selected" disabled>Memiliki telepon rumah</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Televisi</label>
              <select class="form-control" style="width: 100%;"id="Televisi" name="Televisi">
                <option selected="selected" disabled>Memiliki televisi</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Emas/perhiasan & tabungan senilai 10 gr emas</label>
              <select class="form-control" style="width: 100%;"id="Perhiasan10gr" name="Perhiasan10gr">
                <option selected="selected" disabled>Memiliki perhiasan 10 gr</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Komputer/laptop</label>
              <select class="form-control" style="width: 100%;"id="Komputer_Laptop" name="Komputer_Laptop">
                <option selected="selected" disabled>Memiliki komputer atau laptop</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Sepeda</label>
              <select class="form-control" style="width: 100%;"id="Sepeda" name="Sepeda">
                <option selected="selected" disabled>Memiliki sepeda</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Sepeda motor</label>
              <select class="form-control" style="width: 100%;"id="SepedaMotor" name="SepedaMotor">
                <option selected="selected" disabled>Memiliki sepeda motor</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Mobil</label>
              <select class="form-control" style="width: 100%;"id="Mobil" name="Mobil">
                <option selected="selected" disabled>Memiliki mobil</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Perahu</label>
              <select class="form-control" style="width: 100%;"id="Perahu" name="Perahu">
                <option selected="selected" disabled>Memiliki perahu</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Motor tempel</label>
              <select class="form-control" style="width: 100%;"id="MotorTempel" name="MotorTempel">
                <option selected="selected" disabled>Memiliki motor tempel</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Perahu motor</label>
              <select class="form-control" style="width: 100%;"id="PerahuMotor" name="PerahuMotor">
                <option selected="selected" disabled>Memiliki perahu motor</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Kapal</label>
              <select class="form-control" style="width: 100%;"id="Kapal" name="Kapal">
                <option selected="selected" disabled>Memiliki kapal</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group"> <!-- aset tidak bergerak -->
            <h5 class="box-title">Rumah tangga memiliki sendiri aset tidak bergerak</h5>
            <label>Lahan</label>
              <select class="form-control" style="width: 100%;"id="Lahan" name="Lahan">
                <option selected="selected" disabled>Memiliki lahan</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Rumah ditempat lain</label>
              <select class="form-control" style="width: 100%;"id="Rumah" name="Rumah">
                <option selected="selected" disabled>Memiliki rumah</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <h5 class="box-title">Jumlah ternak yang dimiliki (ekor)</h5> <!-- jumlah ternak yang dimiliki -->
              <label>Sapi</label>
              <input type="number" class="form-control" id="Sapi" name="Sapi" placeholder="Jumlah sapi (ekor)">
            </div>

            <div class="form-group">
              <label>Kerbau</label>
              <input type="number" class="form-control" id="Kerbau" name="Kerbau" placeholder="Jumlah Kerbau (ekor)">
            </div>
            
            <div class="form-group">
              <label>Kuda</label>
              <input type="number" class="form-control" id="Kuda" name="Kuda" placeholder="Jumlah kuda (ekor)">
            </div>
            
            <div class="form-group">
              <label>Babi</label>
              <input type="number" class="form-control" id="Babi" name="Babi" placeholder="Jumlah babi (ekor)">
            </div>
            
            <div class="form-group">
              <label>Kambing/domba</label>
              <input type="number" class="form-control" id="Kambing_Domba" name="Kambing_Domba" placeholder="Jumlah kombang atau domba (ekor)">
            </div>
            
            <div class="form-group">
              <label>Apakah ada ART yang memiliki usaha sendiri</label>
              <select class="form-control" style="width: 100%;"id="JenisUsaha" name="JenisUsaha">
                <option selected="selected" disabled>Memiliki usaha sendiri</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Nama pemilik usaha</label>
              <input type="text" class="form-control" id="NamaPemilik" name="NamaPemilik" placeholder="Nama pemilik usaha">
            </div>

            <div class="form-group">
              <label>Nomor urut ART</label>
              <input type="number" class="form-control" id="NomorART" name="NomorART" placeholder="Nomor urut ART">
            </div>

            <div class="form-group">
              <label>Lapangan usaha (ditulis secara lengkap)</label>
              <input type="text" class="form-control" id="LapanganUsaha" name="LapanganUsaha" placeholder="LapanganUsaha">
            </div>

            <div class="form-group">
              <label>Jumlah pekerja (orang)</label>
              <input type="text" class="form-control" id="JmlPekerja" name="JmlPekerja" placeholder="Jumlah pekerjaan">
            </div>

            <div class="form-group">
              <label>Tempat lokasi usaha </label>
              <select class="form-control" style="width: 100%;"id="TempatUsaha" name="TempatUsaha">
                <option selected="selected" disabled>Memiliki tempat usaha yang tetap maupun berpindah-pindah</option>
                <option>1. Ada</option>
                <option>2. Tidak ada</option>
              </select>
            </div>

            <div class="form-group">
              <label>Omset usaha perbulan</label>
              <select class="form-control" style="width: 100%;"id="Omset" name="Omset">
                <option selected="selected" disabled>Masukan jumlah omset usaha yang didapat perbulannya</option>
                <option>1. < 1 juta</option>
                <option>2. 1 - < 5 juta</option>
                <option>3. 5 - < 10 juta</option>
                <option>4. >= 10 juta</option>
              </select>
            </div>

            <div class="form-group">
              <h5 class="box-title"> Rumah tangga menjadi peserta program/memiliki kartu program berikut</h5>
              <label>Kartu Keluarga Sejahtera (KKS)/Kartu perlindungan sosial (KPS) </label> <!-- form program -->
              <select class="form-control" style="width: 100%;"id="KKS" name="KKS">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>

            <div class="form-group">
              <label>Kartu Indonesia Pintar/Bantuan Siswa Miskin </label> 
              <select class="form-control" style="width: 100%;"id="KIP" name="KIP">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>

            <div class="form-group">
              <label>Kartu Indonesia Sehat (KIS)/BPJS Kesehatan/JAMKESMAS </label> 
              <select class="form-control" style="width: 100%;"id="KIS" name="KIS">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>

            <div class="form-group">
              <label>BPJS Kesehatan peserta mandiri</label>
              <select class="form-control" style="width: 100%;"id="BPJS" name="BPJS">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>

            <div class="form-group">
              <label> Jaminan sosial tenaga kerja (JAMSOSTEK)/BPJS Ketenagakerjaan</label>
              <select class="form-control" style="width: 100%;"id="JAMSOSTEK" name="JAMSOSTEK">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>

            <div class="form-group">
              <label> Asuransi kesehatan lainnya</label>
              <select class="form-control" style="width: 100%;"id="Asuransi" name="Asuransi">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>

            <div class="form-group">
              <label> Program keluarga harapan (PKH)</label>
              <select class="form-control" style="width: 100%;"id="PKH" name="PKH">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>
            <div class="form-group">
              <label> Beras untuk orang miskin (RASKIN)</label>
              <select class="form-control" style="width: 100%;"id="RASKIN" name="RASKIN">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>

            <div class="form-group">
              <label> Kredit usaha rakyat (KUR)</label>
              <select class="form-control" style="width: 100%;"id="KUR" name="KUR">
                <option selected="selected" disabled>Memiliki atau menjadi peserta program</option>
                <option>1. Ya </option>
                <option>2. Tidak </option>
              </select>
            </div>
            <button type="cancel" class="btn btn-basic float-left" id="cancel" style="float: left;">Cancel</button>
            <button type="submit" class="btn btn-success float-right" id="submit" style="float: right;">Submit</button>

        
    </form>
    <!-- Akhir Formulir Penanganan Kemiskinan -->
  </section>


  <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
  <script src="<?=base_url()?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="<?=base_url()?>/assets/bower_components/chart.js/Chart.js"></script>
  <script src="<?=base_url()?>/assets/bower_components/morris.js/morris.min.js"></script>
  <!-- Sparkline -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="<?=base_url()?>/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="<?=base_url()?>/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="<?=base_url()?>/assets/bower_components/moment/min/moment.min.js"></script>
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="<?=base_url()?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?=base_url()?>/assets/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?=base_url()?>/assets/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?=base_url()?>/assets/bower_components/chart.js/Chart.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?=base_url()?>/assets/dist/js/pages/dashboard2.js"></script>
  <!-- Select2 -->
  <script src="<?=base_url()?>/assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!--   button next and prev -->
  <script>
    $("#prev").on("click", function(){
      prev();
    });

    $("#next").on("click", function(){
      next();
    });

    // var section = ["PengenalanTempat", "Petugas", "Perumahan", "Ekonomi", "Aset"];
    // var position = 0;

    // function prev(){
    //   if(position == 0){
    //     return true;
    //   }else{
    //     $("#submit").css("display", "none");
    //     $("#next").css("display", "");
    //     $("#"+section[position]).css("display", "none");
    //     position--;
    //     $("#"+section[position]).css("display", "");
    //     if(position == 0){
    //       $("#prev").css("display", "none");
    //     }
    //   }
    // }

    // function next(){
    //   if(position == 5){
    //     return true;
    //   }else{
    //    $("#prev").css("display", "");
    //    $("#"+section[position]).css("display", "none");
    //    position++;
    //    $("#"+section[position]).css("display", "");
    //    if(position == 4){
    //     $("#submit").css("display", "");
    //     $("#next").css("display", "none");
    //   }
    // }
  }
    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- Default to the left -->
      <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
          <h3 class="control-sidebar-heading">Recent Activity</h3>
          <ul class="control-sidebar-menu">
            <li>
              <a href="javascript:;">
                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                  <p>Will be 23 on April 24th</p>
                </div>
              </a>
            </li>
          </ul>
          <!-- /.control-sidebar-menu -->

          <h3 class="control-sidebar-heading">Tasks Progress</h3>
          <ul class="control-sidebar-menu">
            <li>
              <a href="javascript:;">
                <h4 class="control-sidebar-subheading">
                  Custom Template Design
                  <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
                </h4>

                <div class="progress progress-xxs">
                  <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                </div>
              </a>
            </li>
          </ul>
          <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
          <form method="post">
            <h3 class="control-sidebar-heading">General Settings</h3>

            <div class="form-group">
              <label class="control-sidebar-subheading">
                Report panel usage
                <input type="checkbox" class="pull-right" checked>
              </label>

              <p>
                Some information about this general settings option
              </p>
            </div>
            <!-- /.form-group -->
          </form>
        </div>
        <!-- /.tab-pane -->
      </div>
    </aside>
    <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?=base_url()?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?=base_url()?>/assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- InputMask -->
  <script src="<?=base_url()?>/assets/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?=base_url()?>/assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?=base_url()?>/assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- date-range-picker -->
  <script src="<?=base_url()?>/assets/bower_components/moment/min/moment.min.js"></script>
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- bootstrap datepicker -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- bootstrap color picker -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <!-- bootstrap time picker -->
  <script src="<?=base_url()?>/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- iCheck 1.0.1 -->
  <script src="<?=base_url()?>/assets/plugins/iCheck/icheck.min.js"></script>
  <!-- FastClick -->
  <script src="<?=base_url()?>/assets/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?=base_url()?>/assets/dist/js/adminlte.min.js"></script>
//   <script>


//     $("#prev").on("click", function(){
//       prev();
//     });

//     $("#next").on("click", function(){
//       next();
//     });

//     var section = ["data_almarhum", "kecelakaan", "kondisi_kronis", "daftar_keluhan", "konsumsi_tembakau", "rekaman_kesehatan"];
//     var position = 0;

//     function prev(){
//       if(position == 0){
//         return true;
//       }else{
//         $("#submit").css("display", "none");
//         $("#next").css("display", "");
//         $("#"+section[position]).css("display", "none");
//         position--;
//         $("#"+section[position]).css("display", "");
//         if(position == 0){
//           $("#prev").css("display", "none");
//         }
//       }
//     }

//     function next(){
//       if(position == 5){
//         return true;
//       }else{
//        $("#prev").css("display", "");
//        $("#"+section[position]).css("display", "none");
//        position++;
//        $("#"+section[position]).css("display", "");
//        if(position == 5){
//         $("#submit").css("display", "");
//         $("#next").css("display", "none");
//        }
//      }
//    }






//     $(function () {
//     //Initialize Select2 Elements
//     $('.select2').select2()

//     //Datemask dd/mm/yyyy
//     $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
//     //Datemask2 mm/dd/yyyy
//     $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
//     //Money Euro
//     $('[data-mask]').inputmask()

//     //Date range picker
//     $('#reservation').daterangepicker()
//     //Date range picker with time picker
//     $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
//     //Date range as a button
//     $('#daterange-btn').daterangepicker(
//     {
//       ranges   : {
//         'Today'       : [moment(), moment()],
//         'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//         'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
//         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
//         'This Month'  : [moment().startOf('month'), moment().endOf('month')],
//         'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//       },
//       startDate: moment().subtract(29, 'days'),
//       endDate  : moment()
//     },
//     function (start, end) {
//       $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
//     }
//     )

//     //Date picker
//     $('#datepicker').datepicker({
//       autoclose: true
//     })

//     //iCheck for checkbox and radio inputs
//     $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
//       checkboxClass: 'icheckbox_minimal-blue',
//       radioClass   : 'iradio_minimal-blue'
//     })
//     //Red color scheme for iCheck
//     $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
//       checkboxClass: 'icheckbox_minimal-red',
//       radioClass   : 'iradio_minimal-red'
//     })
//     //Flat red color scheme for iCheck
//     $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//       checkboxClass: 'icheckbox_flat-green',
//       radioClass   : 'iradio_flat-green'
//     })

//     //Colorpicker
//     $('.my-colorpicker1').colorpicker()
//     //color picker with addon
//     $('.my-colorpicker2').colorpicker()

//     //Timepicker
//     $('.timepicker').timepicker({
//       showInputs: false
//     })
//   })

//  </script>

</body>
</html>