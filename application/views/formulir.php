<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>BANSOS (Bantuan Sosial)</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/AdminLTE.min.css">
 <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/skins/skin-blue.min.css">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <!--   select2 -->
   <link rel="stylesheet" href="<?=base_url()?>/assets/bower_components/select2/dist/css/select2.min.css">
   <!-- Google Font -->
   <link rel="stylesheet"
   href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <style type="text/css">
     .item img {width: 100%; height: auto}
   </style>
 </head>

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <?php $this->load->view('header');?>

    <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header"> <!--Tampilan menu-->
    <h1>
      Formulir Penanganan Kemiskinan
      <small>Dinas Sosial Kota Malang</small>
    </h1>
  </section>
  
  
  <section class="content container-fluid">
    <!-- Awal Formulir Penanganan Kemiskinan -->
    <form role="form" method="post" action="<?php echo base_url('home/submitFormulir'); ?>">
    <div id="Petugas" style="display: none"> <!-- Form keterangan petugas dan responden -->
        <div class="box box-primary"> 
          <div class="box-header with-border">
            <h3 class="box-title">Keterangan Petugas dan Responden</h3>
          </div>

          <div class="box-body">  <!-- form isian petugas -->
            <div class="form-group">
              <label>Tanggal Pencacahan</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="Tgl_Pencacahan" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="Tgl_Pencacahan">
              </div>
            </div>

            <div class="form-group">
              <label>Nama Pencacah</label>
              <input type="text" class="form-control" id="NamaPencacahan" name="NamaPencacahan" placeholder="Masukan Nama">
            </div>
          </div>
        </div>
      </div>
      <div class="box box-primary"> <!-- Form pengenalan tempat -->
          <div class="box-header with-border">
            <h3 class="box-title">Pengenalan Tempat</h3>
          </div>

          <div class="box-body" >   <!-- form pengenalan tempat -->
            <div class="form-group">
              <label>Kecamatan</label>
              <select class="form-control select2" style="width: 100%;"id="NamaKecamatan" name="Kecamatan">
                <option selected="selected" disabled> Masukan kecamatan</option>
                <option>Kedungkandang</option>
                <option>Klojen</option>
                <option>Lowokwaru</option>
                <option>Sukun</option>
              </select>
            </div>

            <div class="form-group" >
              <label>Kelurahan</label>
              <select class="form-control select2" style="width: 100%;" id="NamaKelurahan" name="Kelurahan">
                <option selected="selected" disabled>Masukan Kelurahan</option>
                <option>Arjosari</option>
                <option>Arjowinangun</option>
                <option>Bakalankrajan</option>
                <option>Balearjosari</option>
                <option>Bandulan</option>
                <option>Bandungrejosari</option>
                <option>Bareng</option>
                <option>Blimbing</option>
                <option>Bumiayu</option>
                <option>Bunulrejo</option>
                <option>Buring</option>
                <option>Cemorokandang</option>
                <option>Ciptomulyo</option>
                <option>Dinoyo</option>
                <option>Gadang</option>
                <option>Gadingasri</option>
                <option>Jatimulyo</option>
                <option>Jodipan</option>
                <option>Karangbesuki</option>
                <option>Kasin</option>
                <option>Kauman</option>
                <option>Kebonsari</option>
                <option>Kedungkandang</option>
                <option>Kesatrian</option>
                <option>Ketawanggede</option>
                <option>Kiduldalem</option>
                <option>Klojen</option>
                <option>Kotalama</option>
                <option>Lesanpuro</option>
                <option>Lowokwaru</option>
                <option>Madyopuro</option>
                <option>Mergosono</option>
                <option>Merjosari</option>
                <option>Mojolangu</option>
                <option>Mulyorejo</option>
                <option>Oro-Oro Dowo</option>
                <option>Pandanwangi</option>
                <option>Penanggungan</option>
                <option>Pisangcandi</option>
                <option>Polehan</option>
                <option>Polowijen</option>
                <option>Purwantoro</option>
                <option>Purwodadi</option>
                <option>Rampal Celaket</option>
                <option>Samaan</option>
                <option>Sawojajar</option>
                <option>Sukoharjo</option>
                <option>Sukun</option>
                <option>Sumbersari</option>
                <option>Tanjungrejo</option>
                <option>Tasikmadu</option>
                <option>Tlogomas</option>
                <option>Tlogowaru</option>
                <option>Tulusrejo</option>
                <option>Tunggulwulung</option>
                <option>Tunjungsekar</option>
                <option>Wonokoyo</option>
              </select>
            </div>

            <div class="form-group">
              <label>Nama SLS (RT)</label>
              <input type="text" class="form-control" id="NamaSLS" name="RT" placeholder="RT">
            </div>
            <div class="form-group">
              <label>Nama SLS (RW)</label>
              <input type="text" class="form-control" id="NamaSLS" name="RW" placeholder="RW">
            </div>
            <div class="form-group">
              <label>Alamat tempat tinggal</label>
              <input type="text" class="form-control" id="Alamat" name="Alamat" placeholder="Masukan Alamat">
            </div>         
            
         
      

      <!-- <div id="Petugas" style="display: none"> 
        <div class="box box-primary"> 
          <div class="box-header with-border">
            <h3 class="box-title">Keterangan Petugas dan Responden</h3>
          </div> -->

            <div class="form-group">
              <label>Tanggal Pencacahan</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="Tgl_Pencacahan" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="Tgl_Pencacahan">
              </div>
            </div>

            <div class="form-group">
              <label>Nama Pencacah</label>
              <input type="text" class="form-control" id="NamaPencacahan" name="NamaPencacahan" placeholder="Masukan Nama">
            </div>

            <div class="form-group">
              <label >Tanggal Pemeriksa</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="TglPemeriksa" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask name="TglPemeriksa">
              </div>
            </div>

            <div class="form-group">
              <label>Nama Pemeriksa</label>
              <input type="text" class="form-control" id="NamaPemeriksa" name="NamaPemeriksa" placeholder="Masukan Nama Pemeriksa">
            </div>
         </div>
      
      <!-- Form keterangan sosial ekonomi anggota rumah tangga -->
      <!-- <div id="Ekonomi" style="display: none" > 
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Keterangan Sosial Ekonomi Anggota Rumah Tangga</h3>
          </div> -->
          
          <!-- form ekonomi -->
          <div class="box-body">  
            <div class="form-group">
              <label>Nama</label><br>
              <input type="text" class="form-control" id="Nama" name="Nama" placeholder="Masukan nama anggota rumah tangga">
            </div>

            <div class="form-group">
              <label>NIK</label>
              <input type="text" class="form-control" id="NIK" name="NIK" placeholder="Masukan NIK anggota rumah tangga">
            </div>
            
            <div class="form-group">
              <label>Jenis kelamin</label>
              <select class="form-control" style="width: 100%;"id="JenisKlamin" name="JenisKlamin">
                <option selected="selected" disabled=> Masukkan jenis kelamin</option>
                <option>1. Laki-Laki</option>
                <option>2. Perempuan</option>
              </select>
            </div>

            <div class="form-group">
              <label>Umur</label>
              <input type="text" class="form-control" id="Umur" name="Umur" placeholder="Masukan umur anda">
            </div>

            <div class="form-group">
              <label>Status perkawinan</label>
              <select class="form-control" style="width: 100%;"id="StatusPerkawinan" name="StatusPerkawinan">
                <option selected="selected" disabled>Masukan status perkawinan</option>
                <option>1. Belum kawin</option>
                <option>2. Kawin/nikah</option>
                <option>3. Cerai hidup</option>
                <option>4. Cerai mati</option>
              </select>
            </div>

            <div class="form-group">
              <label>Status kehamilan untuk wanita usia 10-49 tahun dan Kol(8)=2</label>
              <select class="form-control" style="width: 100%;"id="StatusKehamilan" name="StatusKehamilan">
                <option selected="selected" disabled>Jumlahkan kode yang sesuai</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Jenis cacat</label>
              <select class="form-control" style="width: 100%;"id="JenisCacat" name="JenisCacat">
                <option selected="selected" disabled>Jenis cacat</option>
                <option>00. Tidak cacat</option>
                <option>01. Tuna daksa/cacat tubuh</option>
                <option>02. Tuna netra/buta</option>
                <option>03. Tuna rungu</option>
                <option>04. Tuna wicara</option>
                <option>05. Tuna rungu & wicara</option>
                <option>06. Tuna netra & cacat tubuh</option>
                <option>07. Tuna netra, rungu & wicara</option>
                <option>08. Tuna rungu, wicara & cacat tubuh</option>
                <option>09. Tuna rungu, wicara, netra & cacat tubuh</option>
                <option>10. Cacat mental retardasi</option>
                <option>11. Mantan penderita gangguan jiwa</option>
                <option>12. Cacat fisik & mental</option>
              </select>
            </div>

            <div class="form-group">
              <label>Penyakit kronis/menahun</label>
              <select class="form-control" style="width: 100%;"id="PenyakitKronis" name="PenyakitKronis">
                <option selected="selected" disabled>Jenis penyakit kronis yang diderita</option>
                <option>0. Tidak ada</option>
                <option>1. Hipertensi/tekanan darah tinggi</option>
                <option>2. Rematik</option>
                <option>3. Asma</option>
                <option>4. Masalah jantung</option>
                <option>5. Diabetes (kencing manis)</option>
                <option>6. Tubercolosis (TBC)</option>
                <option>7. Stroke</option>
                <option>8. Kanker atau tumor ganas</option>
                <option>9. Lainnya (gagal ginjal, paru-paru flek dan sejenisnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Partisipasi sekolah</label>
              <select class="form-control" style="width: 100%;"id="PartisipasiSekolah" name="PartisipasiSekolah">
                <option selected="selected" disabled>Partisipasi sekolah</option>
                <option>0. Tidak/belum pernah sekolah</option>
                <option>1. Masih sekolah</option>
                <option>2. Tidak bersekolah lagi</option>
              </select>
            </div>

            <div class="form-group">
              <label>Jenjang dan jenis pendidikan tertinggi yang pernah atau sedang diduduki</label>
              <select class="form-control" style="width: 100%;"id="JenjangPendidikanTertinggi" name="JenjangPendidikanTertinggi">
                <option selected="selected" disabled>Jenjang pendidikan tertinggi</option>
                <option>01. SD/SDLB</option>
                <option>02. Paket A</option>
                <option>03. M. Ibtidaiyah</option>
                <option>04. SMP/SMPLB</option>
                <option>05. Paket B</option>
                <option>06. M. Tsanawiyah</option>
                <option>07. SMA/SMK/SMALB</option>
                <option>08. Paket C</option>
                <option>09. M. Aliyah</option>
                <option>10. Perguruan tinggi</option>
              </select>
            </div>

            <div class="form-group">
              <label>Kelas tertinggi yang pernah atau sedang diduduki</label>
              <select class="form-control" style="width: 100%;"id="KelasTertinggi" name="KelasTertinggi">
                <option selected="selected" disabled>Kelas tertinggi</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8 (tamat)</option>
              </select>
            </div>

            <div class="form-group">
              <label>Ijazah tertinggi yang dimiliki</label>
              <select class="form-control" style="width: 100%;"id="IjazahTertinggi" name="IjazahTertinggi">
                <option selected="selected" disabled>IjazahTertinggi</option>
                <option>0. Tidak punya ijazah</option>
                <option>1. SD/Sederajat</option>
                <option>2. SMP/Sederajat</option>
                <option>3. SMA/Sederajat</option>
                <option>4. D1/D2/D3</option>
                <option>5. D4/S1</option>
                <option>6. S2/S3</option>
              </select>
            </div>

            <div class="form-group">
              <label>Bekerja/membantu bekerja selama seminggu yang lalu</label>
              <select class="form-control" style="width: 100%;"id="StatusBekerja" name="StatusBekerja">
                <option selected="selected" disabled>Status bekerja</option>
                <option>1. Ya</option>
                <option>2. Tidak</option>
              </select>
            </div>

            <div class="form-group">
              <label>Lapangan usaha dari pekerjaan utama</label>
              <select class="form-control" style="width: 100%;"id="LapanganPekerjaan" name="LapanganPekerjaan">
                <option selected="selected" disabled>Masukan jenis lapangan pekerjaan</option>
                <option>1. Pertanian tanaman padi & palawija</option>
                <option>2. Hortikultura</option>
                <option>3. Perkebunan</option>
                <option>4. Perikanan tangkap</option>
                <option>5. Perikanan budidaya</option>
                <option>6. Peternakan</option>
                <option>7. Kehutanan & pertanian lainnya</option>
                <option>8. Pertambangan/penggalian</option>
                <option>9. Industri pengolahan</option>
                <option>10. Listrik dan gas</option>
                <option>11. Bangunan/konstruksi</option>
                <option>12. Perdagangan</option>
                <option>13. Hotel & rumah makan</option>
                <option>14. Transportasi & pergudangan</option>
                <option>15. Informasi & komunikasi</option>
                <option>16. Keuangan & asuransi</option>
                <option>17. Jasa pendidikan</option>
                <option>18. Jasa kesehatan</option>
                <option>19. Jasa kemasyarakatan, pemerintahan & perorangan</option>
                <option>20. Pemulung</option>
                <option>21. Lainnya</option>
              </select>
            </div>

            <div class="form-group">
              <label>Status kedudukan dalam pekerjaan utama</label>
              <select class="form-control" style="width: 100%;"id="StatusKedudukanPekerjaan" name="StatusKedudukanPekerjaan">
                <option selected="selected" disabled>Status kedudukkan</option>
                <option>1. Berusaha sendiri</option>
                <option>2. Berusaha dibantu buruh tidak tetap/tidak dibayar</option>
                <option>3. Berusaha dibantu buruh tetap/ dibayar</option>
                <option>4. Buruh/karyawan/pegawai swasta</option>
                <option>5. PNS/TNI/POLRI/BUMN/BUMD/anggota legislatif</option>
                <option>6. Pekerjaan bebas pertanian</option>
                <option>7. Pekerja bebas non-pertanian</option>
                <option>8. Pekerja keluarga/tidak dibayar</option>
              </select>
            </div>
            <button type="cancel" class="btn btn-basic float-left" id="cancel" style="float: left;">Cancel</button>
            <button type="submit" class="btn btn-success float-right" id="submit" style="float: right;">Submit</button>
          </div>


      
    </form>
    <!-- Akhir Formulir Penanganan Kemiskinan -->
  </section>


  <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
  <script src="<?=base_url()?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="<?=base_url()?>/assets/bower_components/chart.js/Chart.js"></script>
  <script src="<?=base_url()?>/assets/bower_components/morris.js/morris.min.js"></script>
  <!-- Sparkline -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="<?=base_url()?>/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="<?=base_url()?>/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="<?=base_url()?>/assets/bower_components/moment/min/moment.min.js"></script>
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="<?=base_url()?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?=base_url()?>/assets/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?=base_url()?>/assets/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?=base_url()?>/assets/bower_components/chart.js/Chart.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?=base_url()?>/assets/dist/js/pages/dashboard2.js"></script>
  <!-- Select2 -->
  <script src="<?=base_url()?>/assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!--   button next and prev -->
  <script>
    $("#prev").on("click", function(){
      prev();
    });

    $("#next").on("click", function(){
      next();
    });

    // var section = ["PengenalanTempat", "Petugas", "Perumahan", "Ekonomi", "Aset"];
    // var position = 0;

    // function prev(){
    //   if(position == 0){
    //     return true;
    //   }else{
    //     $("#submit").css("display", "none");
    //     $("#next").css("display", "");
    //     $("#"+section[position]).css("display", "none");
    //     position--;
    //     $("#"+section[position]).css("display", "");
    //     if(position == 0){
    //       $("#prev").css("display", "none");
    //     }
    //   }
    // }

    // function next(){
    //   if(position == 5){
    //     return true;
    //   }else{
    //    $("#prev").css("display", "");
    //    $("#"+section[position]).css("display", "none");
    //    position++;
    //    $("#"+section[position]).css("display", "");
    //    if(position == 4){
    //     $("#submit").css("display", "");
    //     $("#next").css("display", "none");
    //   }
    // }
  }
    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- Default to the left -->
      <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane active" id="control-sidebar-home-tab">
          <h3 class="control-sidebar-heading">Recent Activity</h3>
          <ul class="control-sidebar-menu">
            <li>
              <a href="javascript:;">
                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                  <p>Will be 23 on April 24th</p>
                </div>
              </a>
            </li>
          </ul>
          <!-- /.control-sidebar-menu -->

          <h3 class="control-sidebar-heading">Tasks Progress</h3>
          <ul class="control-sidebar-menu">
            <li>
              <a href="javascript:;">
                <h4 class="control-sidebar-subheading">
                  Custom Template Design
                  <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
                </h4>

                <div class="progress progress-xxs">
                  <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                </div>
              </a>
            </li>
          </ul>
          <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
          <form method="post">
            <h3 class="control-sidebar-heading">General Settings</h3>

            <div class="form-group">
              <label class="control-sidebar-subheading">
                Report panel usage
                <input type="checkbox" class="pull-right" checked>
              </label>

              <p>
                Some information about this general settings option
              </p>
            </div>
            <!-- /.form-group -->
          </form>
        </div>
        <!-- /.tab-pane -->
      </div>
    </aside>
    <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->

  <!-- jQuery 3 -->
  <script src="<?=base_url()?>/assets/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?=base_url()?>/assets/bower_components/select2/dist/js/select2.full.min.js"></script>
  <!-- InputMask -->
  <script src="<?=base_url()?>/assets/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?=base_url()?>/assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?=base_url()?>/assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- date-range-picker -->
  <script src="<?=base_url()?>/assets/bower_components/moment/min/moment.min.js"></script>
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- bootstrap datepicker -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- bootstrap color picker -->
  <script src="<?=base_url()?>/assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
  <!-- bootstrap time picker -->
  <script src="<?=base_url()?>/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?=base_url()?>/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- iCheck 1.0.1 -->
  <script src="<?=base_url()?>/assets/plugins/iCheck/icheck.min.js"></script>
  <!-- FastClick -->
  <script src="<?=base_url()?>/assets/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?=base_url()?>/assets/dist/js/adminlte.min.js"></script>
//   <script>


//     $("#prev").on("click", function(){
//       prev();
//     });

//     $("#next").on("click", function(){
//       next();
//     });

//     var section = ["data_almarhum", "kecelakaan", "kondisi_kronis", "daftar_keluhan", "konsumsi_tembakau", "rekaman_kesehatan"];
//     var position = 0;

//     function prev(){
//       if(position == 0){
//         return true;
//       }else{
//         $("#submit").css("display", "none");
//         $("#next").css("display", "");
//         $("#"+section[position]).css("display", "none");
//         position--;
//         $("#"+section[position]).css("display", "");
//         if(position == 0){
//           $("#prev").css("display", "none");
//         }
//       }
//     }

//     function next(){
//       if(position == 5){
//         return true;
//       }else{
//        $("#prev").css("display", "");
//        $("#"+section[position]).css("display", "none");
//        position++;
//        $("#"+section[position]).css("display", "");
//        if(position == 5){
//         $("#submit").css("display", "");
//         $("#next").css("display", "none");
//        }
//      }
//    }






//     $(function () {
//     //Initialize Select2 Elements
//     $('.select2').select2()

//     //Datemask dd/mm/yyyy
//     $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
//     //Datemask2 mm/dd/yyyy
//     $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
//     //Money Euro
//     $('[data-mask]').inputmask()

//     //Date range picker
//     $('#reservation').daterangepicker()
//     //Date range picker with time picker
//     $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
//     //Date range as a button
//     $('#daterange-btn').daterangepicker(
//     {
//       ranges   : {
//         'Today'       : [moment(), moment()],
//         'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//         'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
//         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
//         'This Month'  : [moment().startOf('month'), moment().endOf('month')],
//         'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//       },
//       startDate: moment().subtract(29, 'days'),
//       endDate  : moment()
//     },
//     function (start, end) {
//       $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
//     }
//     )

//     //Date picker
//     $('#datepicker').datepicker({
//       autoclose: true
//     })

//     //iCheck for checkbox and radio inputs
//     $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
//       checkboxClass: 'icheckbox_minimal-blue',
//       radioClass   : 'iradio_minimal-blue'
//     })
//     //Red color scheme for iCheck
//     $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
//       checkboxClass: 'icheckbox_minimal-red',
//       radioClass   : 'iradio_minimal-red'
//     })
//     //Flat red color scheme for iCheck
//     $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//       checkboxClass: 'icheckbox_flat-green',
//       radioClass   : 'iradio_flat-green'
//     })

//     //Colorpicker
//     $('.my-colorpicker1').colorpicker()
//     //color picker with addon
//     $('.my-colorpicker2').colorpicker()

//     //Timepicker
//     $('.timepicker').timepicker({
//       showInputs: false
//     })
//   })

//  </script>

</body>
</html>