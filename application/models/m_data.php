<?php

class m_data extends CI_Model {
	
	function __construct()
	{
	  parent::__construct();
    }
	
	function test()
	{
		return 'Data untuk test model';
	}
	
	function proses_contact($array)
	{
		$nama = $array['nama'];
		$email= $array['email'];
		$telp = $array['telp'];
		$pesan= $array['pesan'];
		$data = 'Nama :'.$nama.'<br />'.
				'Email:'.$email.'<br />'.
				'Telp :'.$telp.'<br />'.
				'Pesan:'.$pesan;
		return $data;
	}
}